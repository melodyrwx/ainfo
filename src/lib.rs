mod airport;
mod key;

extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate serde_json;

use crate::airport::Airport;
use crate::key::ApiKey;
use curl::easy::Easy;
use curl::easy::List;

pub fn api(airport: &str, ct: &str, key: ApiKey) -> Airport {
    let api_path = format!("https://airport-info.p.rapidapi.com/airport?{}={}", ct, airport);
    let header_key = format!("X-RapidAPI-Key: {}", key.key);
    let header_path = format!("X-RapidAPI-Host: {}", key.host);

    let mut api_data = Vec::new();
    {
        let mut headers = List::new();
        headers.append(&header_key).unwrap();
        headers.append(&header_path).unwrap();

        let mut easy = Easy::new();
        easy.url(&api_path).unwrap();
        easy.http_headers(headers).unwrap();

        let mut transfer = easy.transfer();
        transfer.write_function(|data| {
            api_data.extend_from_slice(data);
            Ok(data.len())
        }).unwrap();
        transfer.perform().unwrap();
    }

    let adata = String::from_utf8(api_data).expect("Error");
    let model: Airport = serde_json::from_str(&adata).unwrap();

    return model;
}

// #[cfg(test)]
// mod tests {
//     use std::fmt::format;

//     use super::*;

//     #[test]
//     fn it_works() {
//         // let result = add(2, 2);
//         // assert_eq!(result, 4);
        
//     }
// }
