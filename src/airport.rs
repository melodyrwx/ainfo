extern crate serde_derive;

#[derive(Serialize, Deserialize)]
pub struct Airport {
    pub id: i64,
    pub iata: String,
    pub icao: String,
    pub name: String,
    pub location: String,
    pub street_number: String,
    pub street: String,
    pub city: String,
    pub county: String,
    pub state: String,
    pub country_iso: String,
    pub country: String,
    pub postal_code: String,
    pub phone: String,
    pub latitude: f64,
    pub longitude: f64,
    pub uct: i64,
    pub website: String,
}